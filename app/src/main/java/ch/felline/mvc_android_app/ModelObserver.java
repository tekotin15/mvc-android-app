package ch.felline.mvc_android_app;

/**
 * Created by gfelline on 4/26/17.
 */

public interface ModelObserver {
    void viewModelChanged();
}
