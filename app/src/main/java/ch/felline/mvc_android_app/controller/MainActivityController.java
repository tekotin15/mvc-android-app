package ch.felline.mvc_android_app.controller;

import ch.felline.mvc_android_app.model.Counter;

/**
 * Created by gfelline on 4/26/17.
 */

public class MainActivityController {
    final Counter model;

    public MainActivityController(final Counter model) {
        this.model = model;
    }

    public void incrementClicked() {
        model.increment();
    }

    public void resetClicked() {
        model.reset();
    }
}
