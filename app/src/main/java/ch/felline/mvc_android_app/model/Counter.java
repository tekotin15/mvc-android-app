package ch.felline.mvc_android_app.model;

import ch.felline.mvc_android_app.ModelObserver;

/**
 * Created by gfelline on 4/26/17.
 */

public class Counter {
    private int value = 50;
    private ModelObserver observer;

    public Counter(final ModelObserver observer) {
        this.observer = observer;
    }

    public void increment() {
        ++value;
        observer.viewModelChanged();
    }

    public void reset() {
        value = -1;
        observer.viewModelChanged();
    }

    public int value() {
        return value;
    }
}
