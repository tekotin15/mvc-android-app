package ch.felline.mvc_android_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ch.felline.mvc_android_app.controller.MainActivityController;
import ch.felline.mvc_android_app.model.Counter;

public class MainActivity extends AppCompatActivity implements ModelObserver {

    private final Counter model = new Counter(this);
    private final MainActivityController controller = new MainActivityController(model);
    private Button btnIncrement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnIncrement = (Button) findViewById(R.id.btnIncrement);
    }

    public void increment(View v) {
        controller.incrementClicked();
    }

    public void reset(View v) {
        controller.resetClicked();
    }

    @Override
    public void viewModelChanged() {
        btnIncrement.setText(String.valueOf(model.value()));
    }
}
